import urllib.request
from bs4 import BeautifulSoup
import unittest


def read_website_as_string(link):
    try:
        fp = urllib.request.urlopen(link)
        mybytes = fp.read()

        mystr = mybytes.decode("utf8")
        fp.close()

        return mystr
    except (ConnectionRefusedError, urllib.error.URLError):
        return None


def read_div_for_tests_p_name(site):
    if site is None:
        return None
    soup = BeautifulSoup(site, 'html.parser')
    div = BeautifulSoup(str(soup.find(id='for_tests')), 'html.parser')
    name_html = BeautifulSoup(str(div.find(id='name')), 'html.parser')
    return name_html.string


class TestNameSimply(unittest.TestCase):

    def test_name_not_provided(self):
        website = read_website_as_string("http://localhost:8090/")
        name_val = read_div_for_tests_p_name(website)
        self.assertEqual("name is not provide!", name_val)

    def test_name_empty(self):
        website = read_website_as_string("http://localhost:8090/?name")
        name_val = read_div_for_tests_p_name(website)
        self.assertEqual("name is not provide!", name_val)

    def test_admin_krzysztof(self):
        website = read_website_as_string("http://localhost:8090/?name=Krzysztof")
        name_val = read_div_for_tests_p_name(website)
        self.assertEqual("Hello my admin: Krzysztof", name_val)

    def test_normal_name(self):
        website = read_website_as_string("http://localhost:8090/?name=Marta")
        name_val = read_div_for_tests_p_name(website)
        self.assertEqual("Hello Marta", name_val)


if __name__ == '__main__':
    unittest.main()
